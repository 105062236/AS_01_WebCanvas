# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed


Report
    1. 字形及圖形顏色: 點選黑色實心正方形後，即可選擇其他種顏色，共48種。
    2. 畫筆大小       :利用下拉式選單選擇。
    3. 字形選擇       :同樣利用下拉式選單選擇。
    4. 字形大小       :同上。
    5. Pen           :為canvas的默認設定，就是一隻普通的畫筆。
    6. Erase         :箱皮擦大小為30px，可擦掉畫布上所有的東西。
    7. Text          :點選後，在canvas上雙擊，即可打字。
    8.  Drowrectangle:可畫出實心的長方形。
    9.  Drowtriangle :可畫出實心的三角形。
    10. Drowrcircle  :可畫出實心的橢圓形。
    11. Undo         :可回到上一次畫筆放下時的狀態。
    12. Redo         :與Undo相反，回到下次放下畫筆時的狀態。
    13. Reset        :清空畫布上的圖案，回到默認設定。

