var canvas = $('#canvas');
var context = canvas[0].getContext('2d');
var befCol;
var befLine;
var befbtn = '0';
var cursorBias = 26;

var start = false;

var clones = [];

context.fontSize = '14px';
context.fontFace = 'sans-serif';

var startX, starY;

window.onload = function() {
    cPush();
};


var drawThing = class{
    constructor(){
        drawing = false;
        erase = false;
        textInput = false;
        rectanglePush = false;
        circlePush = false;
        trianglePush = false;
    }
}

function EventEmitter() {
    this.events = {};
}
EventEmitter.prototype.on = function(name, cb) {
    if(typeof this.events[name] !== 'undefined') {
        this.events[name] = [];
    }
    this.events[name].push(cb);
};

function button(num){
    if(befbtn !== num){
        clear();
    }
    switch(num){
        case '0':
            befbtn = '0';
            document.getElementById("canvas").style.cursor = "url(icons8-ball-point-pen-26.png), auto";
            cursorBias = 26;
        break
        case '1':
            befCol = context.strokeStyle;
            befLine = context.lineWidth;
            context.lineWidth = 30;
            context.strokeStyle = "#FFFFFF";
            drawThing.erase = true;
            befbtn = '1';

            document.getElementById("canvas").style.cursor = "url(icons8-eraser-26.png), auto";
            cursorBias = 26;
        break
        case '2':
            drawThing.textInput = true;

            befbtn = '2';
            document.getElementById("canvas").style.cursor = 'text';
            //console.log(context.fontFace);
            //console.log(context.fontSize);

        break
        case '3':
            context.clearRect(0, 0, 640, 480);
            clear();
            document.getElementById("canvas").style.cursor = "url(icons8-ball-point-pen-26.png), auto";
            befbtn = '3';
        break
        case '4'://rec
            document.getElementById("canvas").style.cursor = "url(icons8-rectangle-26.png), auto";
            cursorBias = 26;
            befbtn = '4';
        break
        case '5'://tri
            document.getElementById("canvas").style.cursor = "url(icons8-triangle-26.png), auto";
            cursorBias = 26;
            befbtn = '5';
        break
        case '6'://cir
            document.getElementById("canvas").style.cursor = "url(icons8-circle-26.png), auto";
            cursorBias = 26;
            befbtn = '6';
        break
        case '7':
            cUndo();
            document.getElementById("canvas").style.cursor = 'url(icons8-ball-point-pen-26.png), auto';
        break

        case '8':
            cRedo();  
            document.getElementById("canvas").style.cursor = 'url(icons8-ball-point-pen-26.png), auto';
        break
    }
}


function addInput(x, y) {
    
    var input = document.createElement('input');
    
    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = x  + 'px';
    input.style.top = y + 'px';

    input.onkeydown = handleEnter;
    
    document.body.appendChild(input);

    input.focus();
    
    hasInput = true;
}

function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {
        drawText(this.value, (parseInt(this.style.left, 10)) + 16, parseInt(this.style.top, 10)) - 4;
        document.body.removeChild(this);
    }
}

function drawText(txt, x, y) {
    context.textBaseline = 'top';
    context.textAlign = 'left';
    context.font = context.fontSize + ' ' + context.fontFace;
    context.fillText(txt, x , y);
}



$(document).ready(function() {
    
    var input = false;
    var queue = [];
    context.lineCap = 'round';
    context.lineJoin = 'round';


    $('#color').colorPicker({
        onColorChange: function(id, val) {
            clear();
            context.strokeStyle = val;
            context.fillStyle = val;
            
        }
    });

    canvas.bind('mousedown', function(e) {
        e.preventDefault();
        if(!drawThing.drawing && !drawThing.textInput) {
            $(this).data('cursor', $(this).css('cursor'));
            drawThing.drawing = true;
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top + cursorBias;
            drawLine(context,x,y,x,y);
            queue.push([x,y]);

            if(befbtn == '4' || befbtn == '5' || befbtn == '6'){
                startX = x;
                startY = y;
                if(befbtn == '4') drawThing.rectanglePush = true;
                if(befbtn == '5') drawThing.trianglePush  = true;
                if(befbtn == '6') drawThing.circlePush    = true;
            }
            var imgData=context.getImageData(0,0,640,480);
            clones.pop();
            clones.push(imgData);
        }
    });
    canvas.bind('mousemove', function(e) {
        e.preventDefault();
        if(drawThing.drawing) {
            context.putImageData(clones[0], 0, 0);

            var old = queue.shift();
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top+ cursorBias;
            if( (befbtn == '4' && drawThing.rectanglePush) || 
                (befbtn == '5' && drawThing.trianglePush)  ||
                (befbtn == '6' && drawThing.circlePush)    ){

                if(befbtn == '4'){
                    context.fillRect(startX, startY, x-startX, y-startY);
                }
                else if(befbtn == '5'){
                    context.beginPath();
                    context.moveTo(2 * startX - x, y);
                    context.lineTo(startX, startY);
                    context.lineTo(x, y);
                    context.fill();
                }  
                else {
                    var radius = Math.sqrt(Math.pow(startX - x, 2) + Math.pow(startY - y, 2), 2);
                    context.beginPath();
                    context.arc(x, y, radius, 0, 2*Math.PI, true);
                    context.closePath();
                    context.fill();
                }
            }
            
            else{
                drawLine(context,old[0],old[1],x,y);
                queue.push([x,y]);
                //存入更新後影像
                var imgData=context.getImageData(0,0,640,480);
                clones.pop();
                clones.push(imgData);
            }
            
            
        }
        
    });
    canvas.bind('mouseup', function(e) {
        if(drawThing.drawing) {
            $(this).css('cursor', $(this).data('cursor'));
            var old = queue.shift();
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top + cursorBias;
            if(befbtn == '4'){
                context.fillRect(startX,startY,x-startX,y-startY);
                
                clear();
            }
            else if(befbtn == '5'){
                context.beginPath();
                context.moveTo(2 * startX - x, y);
                context.lineTo(startX, startY);
                context.lineTo(x, y);
                context.fill();
                
                clear();
            }
            else if(befbtn == '6'){
                var radius = Math.sqrt(Math.pow(startX - x, 2) + Math.pow(startY - y, 2), 2);
                context.beginPath();
                context.arc(x, y, radius, 0, 2*Math.PI, true);
                context.closePath();
                context.fill();
                
                clear();
            }
            else {
                drawLine(context,old[0], old[1], x, y);
                drawThing.drawing = false;
            }

            var imgData=context.getImageData(0,0,640,480);
            clones.pop();
            clones.push(imgData);
            context.putImageData(clones[0], 0, 0);
            
        }
        cPush();
        
    });
    canvas.bind('dblclick', function(e) {
        
        if(drawThing.textInput) {
            addInput(e.pageX, e.pageY);
        }
    });
    canvas.bind('mouseleave', function(e) {
        if(drawThing.drawing) {
            $(this).css('cursor', $(this).data('cursor'));
            var old = queue.shift();
            var offset = $(e.currentTarget).offset();
            var x = e.pageX - offset.left;
            var y = e.pageY - offset.top;
            if(befbtn == '4'){
                context.fillRect(startX,startY,x-startX,y-startY);
                clear();
            }
            if(befbtn == '5'){
                context.beginPath();
                context.moveTo(2 * startX - x, y);
                context.lineTo(startX, startY);
                context.lineTo(x, y);
                context.fill();
                clear();
            }
            if(befbtn == '6'){
                var radius = Math.sqrt(Math.pow(startX - x, 2) + Math.pow(startY - y, 2), 2);
                context.beginPath();
                context.arc(x, y, radius, 0, 2*Math.PI, true);
                context.closePath();
                context.fill();
                clear();
            }
            else {
                drawLine(context,old[0], old[1], x, y);
                drawThing.drawing = false;
            }
        }
    });

    $('#linewidth').bind('change', function() {
        clear();
        context.lineWidth = $(this).val();
    });
    $('#fontsize').bind('change', function() {
        context.fontSize = $(this).val();
        
    });
    $('#fontface').bind('change', function() {
        context.fontFace = $(this).val();
        //console.log($(this).val());
    });

    function drawLine(ctx, x, y, x1, y1) {
        ctx.beginPath();
        ctx.moveTo(x,y);
        ctx.lineTo(x1,y1);
        ctx.closePath();
        ctx.stroke();
    }
});

function clear(){
        context.strokeStyle = befCol;
        context.lineWidth = befLine;
        drawThing.erase = false;
        drawThing.drawing = false;
        drawThing.textInput = false;
        

        drawThing.rectanglePush = false;
        drawThing.circlePush = false;
        drawThing.trianglePush = false;

        cursorBias = 26;

        startX = 0;
        startY = 0;


}

var cPushArray = new Array();
var cStep = -1;
     
function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }

    var c = document.getElementById('canvas');
    
    cPushArray.push(c.toDataURL());
    
}  

function cUndo() {
    if (cStep > -1) {
        console.log(cStep);
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        console.log(cPushArray[cStep]);
        canvasPic.onload = function () { 
            context.clearRect(0,0,640,480);
            context.drawImage(canvasPic, 0, 0); }
    }
}    

function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { context.drawImage(canvasPic, 0, 0); }
    }
}   